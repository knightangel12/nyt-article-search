package schalker.newyorkarticlesearcher.repository;

import javax.inject.Inject;

import io.reactivex.Single;
import schalker.newyorkarticlesearcher.repository.models.SearchResponse;
import schalker.newyorkarticlesearcher.repository.network.NetworkData;

public class DataRepository implements IRepository {
    @Inject
    NetworkData networkData;

    @Inject
    DataRepository() {

    }

    @Override
    public Single<SearchResponse> search(String searchTerm, int page) {
        return networkData.search(searchTerm, page);
    }
}
