package schalker.newyorkarticlesearcher.repository;

import io.reactivex.Single;
import schalker.newyorkarticlesearcher.repository.models.SearchResponse;

public interface IRepository {
    Single<SearchResponse> search(String searchTerm, int page);
}
