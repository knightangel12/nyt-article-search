package schalker.newyorkarticlesearcher.repository.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResponse {
    private String status;
    private Response response;


    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return response;
    }

    public class Response {
        @SerializedName("docs") List<Article> articles;

        public List<Article> getArticles() {
            return articles;
        }
    }
}
