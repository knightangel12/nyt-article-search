package schalker.newyorkarticlesearcher.repository.models;

public class Multimedia {
    private String type;
    private String subtype;
    private String url;

    public String getType() {
        return type;
    }

    public String getSubtype() {
        return subtype;
    }

    public String getUrl() {
        return url;
    }
}
