package schalker.newyorkarticlesearcher.repository.models;

public class Headline {
    private String main;
    private String printHeadline;

    public String getMain() {
        return main;
    }

    public String getPrintHeadline() {
        return printHeadline;
    }
}
