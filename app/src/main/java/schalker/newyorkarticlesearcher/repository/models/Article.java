package schalker.newyorkarticlesearcher.repository.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Article {
    private String webUrl;
    private String snippet;
    private List<Multimedia> multimedia;
    private Headline headline;

    public String getWebUrl() {
        return webUrl;
    }

    public String getSnippet() {
        return snippet;
    }

    public List<Multimedia> getMultimedia() {
        return multimedia;
    }

    public boolean hasThumbnailImage() {

        for(Multimedia m: multimedia) {
            if(m.getType().equals("image") && m.getSubtype().equals("xlarge")) {
                return !m.getUrl().isEmpty();
            }
        }

        return false;
    }

    public String getThumbNailImageUrl() {
        for(Multimedia m: multimedia) {
            if(m.getType().equals("image") && m.getSubtype().equals("xlarge")) {
                return "http://www.nytimes.com/" + m.getUrl();
            }
        }

        return "";
    }

    public Headline getHeadline() {
        return headline;
    }
}
