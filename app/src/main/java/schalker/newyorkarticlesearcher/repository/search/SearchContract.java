package schalker.newyorkarticlesearcher.repository.search;

import java.util.List;

import schalker.newyorkarticlesearcher.BasePresenter;
import schalker.newyorkarticlesearcher.BaseView;
import schalker.newyorkarticlesearcher.repository.models.Article;

public class SearchContract {
    interface View extends BaseView{
        void showArticles(List<Article> articles);
        void showLoading();
        void hideLoading();
        void showError(String errorMessage);
    }

    interface Presenter extends BasePresenter<SearchActivity>{
        void search(String query, int page);
        void openArticleDetails(Article article);
    }
}
