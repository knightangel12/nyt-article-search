package schalker.newyorkarticlesearcher.repository.search;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import schalker.newyorkarticlesearcher.repository.DataRepository;
import schalker.newyorkarticlesearcher.repository.models.Article;

public class SearchPresenter implements SearchContract.Presenter{

    @Inject DataRepository dataRepository;
    @Inject CompositeDisposable disposables;
    private SearchActivity view;

    @Inject
    public SearchPresenter () {

    }

    @Override
    public void takeView(SearchActivity view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        disposables.clear();
    }

    @Override
    public void search(String query, int page) {
        Disposable disposable = dataRepository.search(query, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(searchResponse -> view.showArticles(searchResponse.getResponse().getArticles()),
                        throwable -> view.showError(throwable.getMessage() + ":" + throwable.getLocalizedMessage()));

        disposables.add(disposable);
    }

    @Override
    public void openArticleDetails(Article article) {

    }
}
