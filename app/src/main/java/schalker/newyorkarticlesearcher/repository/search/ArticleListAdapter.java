package schalker.newyorkarticlesearcher.repository.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import schalker.newyorkarticlesearcher.R;
import schalker.newyorkarticlesearcher.repository.models.Article;


class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ArticleViewHolder> {
    private List<Article> articles;
    private Context context;

    //TODO:Figure out how to inject this
    ArticleListAdapter(Context context, List<Article> articles) {
        this.articles = articles;
        this.context = context;
    }

    static class ArticleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.article_image) ImageView articleImage;
        @BindView(R.id.article_headline) TextView headlineView;

        public ArticleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_list_item, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        if(articles.get(position).hasThumbnailImage()) {
            Picasso.with(context).load(articles.get(position).getThumbNailImageUrl()).into(holder.articleImage);
        } else {
            holder.articleImage.setVisibility(View.GONE);
            holder.headlineView.setTextSize(18);
        }

        holder.headlineView.setText(articles.get(position).getHeadline().getMain());
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    private void setList(List<Article> articles) {
        this.articles = articles;
    }

    void replaceData(List<Article> articles) {
        this.articles.addAll(articles);
        setList(this.articles);
        notifyDataSetChanged();
    }

    void clearData() {
        this.articles = new ArrayList<>();
    }
}
