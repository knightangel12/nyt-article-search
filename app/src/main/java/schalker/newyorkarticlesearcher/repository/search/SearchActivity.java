package schalker.newyorkarticlesearcher.repository.search;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import schalker.newyorkarticlesearcher.App;
import schalker.newyorkarticlesearcher.R;
import schalker.newyorkarticlesearcher.repository.models.Article;
import schalker.newyorkarticlesearcher.util.EndlessRecyclerViewScrollListener;

public class SearchActivity extends AppCompatActivity implements  SearchContract.View{

    @Inject
    SearchPresenter presenter;

    @BindView(R.id.articles_list) RecyclerView articleRecyclerView;
    ArticleListAdapter articleListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((App)getApplicationContext()).getApplicationComponent().inject(this);
        ButterKnife.bind(this);

        handleIntent(getIntent());

        articleListAdapter = new ArticleListAdapter(this, new ArrayList<Article>(0));

        articleRecyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        articleRecyclerView.setLayoutManager(manager);
        articleRecyclerView.setAdapter(articleListAdapter);

        articleRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                String query = getIntent().getStringExtra(SearchManager.QUERY);
                presenter.search(query, page);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.dropView();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    public void showArticles(List<Article> articles) {
        articleListAdapter.replaceData(articles);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            articleListAdapter.clearData();
            presenter.search(query, 1);
        }
    }
}


