package schalker.newyorkarticlesearcher.repository.network;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import schalker.newyorkarticlesearcher.repository.models.SearchResponse;

public interface NewYorkTimesService {
    @GET("articlesearch.json")
    public Single<SearchResponse> searchArticles(@Query("q") String query, @Query("api-key") String apiKey, @Query("page") int page);
}
