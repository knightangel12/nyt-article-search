package schalker.newyorkarticlesearcher.repository.network;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Retrofit;
import schalker.newyorkarticlesearcher.BuildConfig;
import schalker.newyorkarticlesearcher.repository.IRepository;
import schalker.newyorkarticlesearcher.repository.models.SearchResponse;

public class NetworkData implements IRepository {
    @Inject
    Retrofit retrofit;

    @Inject
    public NetworkData() {}

    @Override
    public Single<SearchResponse> search(String searchTerm, int page) {
        return retrofit.create(NewYorkTimesService.class)
                .searchArticles(searchTerm, BuildConfig.API_KEY, page);
    }
}
