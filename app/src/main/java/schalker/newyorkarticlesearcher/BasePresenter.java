package schalker.newyorkarticlesearcher;

public interface BasePresenter<T> {
    void takeView(T View);
    void dropView();
}
