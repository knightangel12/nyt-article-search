package schalker.newyorkarticlesearcher.dagger.component;

import javax.inject.Singleton;

import dagger.Component;
import schalker.newyorkarticlesearcher.repository.search.SearchActivity;
import schalker.newyorkarticlesearcher.dagger.module.ApplicationModule;
import schalker.newyorkarticlesearcher.dagger.module.DataModule;

@Singleton
@Component(modules = {ApplicationModule.class, DataModule.class})
public interface ApplicationComponent {
    void inject(SearchActivity activity);
}
