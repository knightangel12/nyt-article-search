package schalker.newyorkarticlesearcher;

import android.app.Application;

import schalker.newyorkarticlesearcher.dagger.component.ApplicationComponent;
import schalker.newyorkarticlesearcher.dagger.component.DaggerApplicationComponent;
import schalker.newyorkarticlesearcher.dagger.module.ApplicationModule;
import schalker.newyorkarticlesearcher.dagger.module.DataModule;


public class App extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new DataModule("https://api.nytimes.com/svc/search/v2/"))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
